package com.kpi;

import org.jzy3d.chart.AWTChart;
import org.jzy3d.colors.ColorMapper;
import org.jzy3d.colors.colormaps.ColorMapHotCold;
import org.jzy3d.javafx.JavaFXChartFactory;
import org.jzy3d.maths.Range;
import org.jzy3d.plot3d.builder.Builder;
import org.jzy3d.plot3d.builder.Mapper;
import org.jzy3d.plot3d.primitives.Shape;
import org.jzy3d.plot3d.rendering.canvas.Quality;

public class Graph {
    private double a1;
    private double b1;
    private float rang;
    private int step;

    public void setRang(float rang) {
        this.rang = rang;
    }

    public void setStep(int step) {
        this.step = step;
    }

    public Graph(double a1, double b1) {
        this.a1 = a1;
        this.b1 = b1;
    }

    public AWTChart graphicChart(JavaFXChartFactory factory, String toolkit){
        Mapper mapper = new Mapper() {
            @Override
            public double f(double x, double y) {
                return 1 - (Math.pow((x - a1), 2)/a1 + Math.pow((y - b1), 2)/b1);
            }
        };

        Range range = new Range(rang, -rang);

        Shape surface = Builder.buildOrthonormal(mapper, range, step);
        surface.setColorMapper(new ColorMapper(new ColorMapHotCold(), surface.getBounds().getZmin(),
                surface.getBounds().getZmax()));
        surface.setFaceDisplayed(true);
        surface.setWireframeDisplayed(false);

        Quality quality = Quality.Advanced;

        AWTChart awtChart = (AWTChart) factory.newChart(quality, toolkit);
        awtChart.getScene().getGraph().add(surface);
        return awtChart;
    }
}
