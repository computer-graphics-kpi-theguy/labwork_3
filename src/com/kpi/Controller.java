package com.kpi;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import org.jzy3d.chart.AWTChart;
import org.jzy3d.javafx.JavaFXChartFactory;


public class Controller {

    @FXML
    private TextField rangeField;
    @FXML
    private TextField stepsField;
    @FXML
    private TextField AField;
    @FXML
    private TextField BField;
    @FXML
    private AnchorPane paneDrawer;

    public void drawClick(ActionEvent actionEvent) {
        float tempRange = Float.parseFloat(rangeField.getText());
        int tempSteps = Integer.parseInt(stepsField.getText());
        double tempA = Double.parseDouble(AField.getText());
        double tempB = Double.parseDouble(BField.getText());

        Graph graph = new Graph(tempA, tempB);
        graph.setRang(tempRange);
        graph.setStep(tempSteps);

        JavaFXChartFactory factory = new JavaFXChartFactory();
        AWTChart chart = graph.graphicChart(factory, "offscreen");
        ImageView image = factory.bindImageView(chart);

        paneDrawer.getChildren().add(image);
    }

    public void clearClick(ActionEvent actionEvent) {
        paneDrawer.getChildren().clear();
    }
}
